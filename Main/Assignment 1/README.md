How to build and run:

- Please make sure that CMake (and preferably the Ninja build system) are
installed on your system.

- If you don't have Ninja, Run the following:

```
mkdir build
cd build
cmake ..
cmake --build .
```

- Otherwise run the following after activating the developer command prompt:

```
mkdir build
cd build
cmake -G Ninja ..
cmake --build .
```

- On Linux:

```
mkdir build
mpicc matrix_cholesky.c -o build/a.out -lm
```

Then cd into build and run:
mpiexec -n 3 a.exe
