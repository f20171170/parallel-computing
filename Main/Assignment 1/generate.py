import numpy as np

matrixSize = 11
A = np.random.rand(matrixSize, matrixSize)
A = 0.5 * (A + A.transpose())
A = A + matrixSize*np.identity(matrixSize)

with open("input.txt", "w+") as f:
    f.write(f"{matrixSize}\n")
    for row in A:
        for element in row:
            f.write(f"{element} ")
        f.write("\n")