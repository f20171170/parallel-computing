#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>

#include "mpi.h"

size_t n;

float **allocate_matrix(size_t n) {
	float **A = malloc(sizeof(float *) * n);
	for (int i = 0; i < n; i++) {
		A[i] = malloc(sizeof(float) * n);
	}
	return A;
}

void free_matrix(float **A) {
	if (A == NULL)
		return;
	for (int i = 0; i < n; i++) {
		free(A[i]);
	}
	free(A);
}

void print_matrix(float **A) {
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++) {
			printf("%.3f ", A[i][j]);
		}
		printf("\n");
	}
}

float **read_input() {
	float **A;
	FILE *f = fopen("../input.txt", "r");
	if (f == NULL) {
		printf("Failed to find ..\\input.txt, aborting.\n");
		exit(EXIT_FAILURE);
	}
	fscanf(f, "%zu", &n);
	A = allocate_matrix(n);
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++) {
			fscanf(f, "%f", &A[i][j]);
		}
	}
	fclose(f);
	f = NULL;
	return A;
}

void report_invalid_matrix() {
	printf("Invalid input matrix; The diagonals must never become 0.\n");
}

int parallel_cholesky(int argc, char **argv) {
	MPI_Init(&argc, &argv);
	int world_rank, world_size;
	MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
	MPI_Comm_size(MPI_COMM_WORLD, &world_size);

	// Read input.
	float **A = read_input();

	// If the number of processes is greater than n, then "discard" the excess
	// processes.
	if (world_rank > n - 1)
		goto clean_up;

	world_size = min(world_size, n);

	// Determine which rows are a part of the block this process is responsible
	// for.
	size_t block_size = n / world_size;
	size_t last_block_size = block_size + n % world_size;
	size_t start_row = world_rank * block_size;
	size_t end_row; // Exclusive. Don't actually operate on this row.
	if (world_rank != world_size - 1) {
		end_row = start_row + block_size;
	} else {
		end_row = start_row + last_block_size;
	}

	// For processes other than the first one, receive the upper rows before we
	// can begin modifying any upper-triangular element in its rows. We can
	// set the other elements to 0 in the meanwhile.
	if (world_rank != 0) {
		for (int k = start_row; k < end_row; k++) {
			for (int j = 0; j < k; j++) {
				A[k][j] = 0;
			}
		}
		for (int k = 0; k < start_row; k++) {
			MPI_Recv(A[k], n, MPI_FLOAT, MPI_ANY_SOURCE, k,
				 MPI_COMM_WORLD, MPI_STATUS_IGNORE);
			for (int i = start_row; i < end_row; i++) {
				for (int j = i; j < end_row; j++) {
					A[i][j] = A[i][j] - (A[k][i] * A[k][j]);
				}
			}
		}
	}

	for (int k = start_row; k < end_row; k++) {
		A[k][k] = sqrt(A[k][k]);
		for (int j = k + 1; j < n; j++) {
			if (A[k][k] == 0) {
				// The input matrix cannot undergo cholesky factorization since
				// it didn't meet one of the prerequisites.
				// The others prerequisites like symmetric and positive
				// definite are not checked in this code.
				report_invalid_matrix();
				MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE);
			}
			A[k][j] = A[k][j] / A[k][k];
		}
		for (int j = 0; j < k; j++) {
			A[k][j] = 0;
		}

		// Send to rows in other blocks before modifying other rows in own
		// block.
		for (int i = world_rank + 1; i < world_size; i++) {
			MPI_Send(A[k], n, MPI_FLOAT, i, k, MPI_COMM_WORLD);
		}

		// Now modify the other rows in our block.
		for (int i = k + 1; i < end_row; i++) {
			for (int j = i; j < n; j++) {
				A[i][j] = A[i][j] - (A[k][i] * A[k][j]);
			}
		}
	}

	// For simplicity sake, let the origin process do all of the output
	// collecting and displaying. It shouldn't affect the complexity by
	// too much since while one block is reporting it's results, another
	// is performing some computation (unless it's the last one).
	// NOTE: This block could be optimized by sending/receiving blocks
	// instead of rows.
	if (world_rank == 0) {
		for (int k = end_row; k < n; k++) {
			MPI_Recv(A[k], n, MPI_FLOAT, MPI_ANY_SOURCE, k,
				 MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		}
		print_matrix(A);
	} else {
		for (int k = start_row; k < end_row; k++) {
			MPI_Send(A[k], n, MPI_FLOAT, 0, k, MPI_COMM_WORLD);
		}
	}

clean_up:
	// Clean up.
	free_matrix(A);
	A = NULL;
	MPI_Finalize();

	if (world_rank == 0)
		return 0;
	return -1;
}

int linear_cholesky(int argc, char **argv) {
	float **A = read_input();
	for (int k = 0; k < n; k++) {
		A[k][k] = sqrt(A[k][k]);
		for (int j = k + 1; j < n; j++) {
			if (A[k][k] == 0) {
				report_invalid_matrix();
				return 1;
			}
			A[k][j] = A[k][j] / A[k][k];
		}
		for (int j = 0; j < k; j++) {
			A[k][j] = 0;
		}
		for (int i = k + 1; i < n; i++) {
			for (int j = i; j < n; j++) {
				A[i][j] = A[i][j] - (A[k][i] * A[k][j]);
			}
		}
	}
	print_matrix(A);
	return 0;
}

int main(int argc, char **argv) {
	clock_t start = clock();
	// linear_cholesky(argc, argv);
	int origin = parallel_cholesky(argc, argv);
	clock_t end = clock();
	double cpu_time_used = ((double)(end - start)) / CLOCKS_PER_SEC;
	if (origin == 0)
		printf("Time taken: %.15lf\n", cpu_time_used);
	return 0;
}