How to build and run:
    build: make
    run (example): mpiexec -n 4 bin/exponential_MPI -x 10

Profile with:
    python3 profile.py

Test machine:
    CPU: Intel(R) Core(TM) i7-8550U CPU @ 1.80GHz
    # of Cores: 8
    RAM: 16GB
    OS: Windows 10

Timings:
    N=1: time taken = 0.016288500    speedup = 1.0000    efficiency = 100.0000
    N=2: time taken = 0.008977000    speedup = 1.8145    efficiency = 90.7235
    N=3: time taken = 0.006644800    speedup = 2.4513    efficiency = 81.7105
    N=4: time taken = 0.005336000    speedup = 3.0526    efficiency = 76.3142
    N=5: time taken = 0.005647500    speedup = 2.8842    efficiency = 57.6839
    N=6: time taken = 0.004774900    speedup = 3.4113    efficiency = 56.8546
    N=7: time taken = 0.004789400    speedup = 3.4009    efficiency = 48.5850
    N=8: time taken = 0.005753900    speedup = 2.8309    efficiency = 35.3858
    N=9: time taken = 0.006179100    speedup = 2.6361    efficiency = 29.2896
    N=10: time taken = 0.006190100    speedup = 2.6314    efficiency = 26.3138

The reason the time taken increases at N=7, 8, 9 and 10 is because of the
architecture of the machine I am performing this computation on (and parhaps
due to background tasks). I am testing this code on an 8-core machine (so 1
core will ideally be reserved for the OS process(es) and 1 more core will be
reserved for my profiling process to run while the remaining free processes
can be used in parallel to execute the code under consideration).