#!/usr/bin/python3

import os
import math
import subprocess

X = 25
NUM_TRIALS = 10
EPSILON = 0.001

ROOT_PATH = os.path.dirname(os.path.dirname(__file__))
EXECUTABLE_PATH = os.path.join(ROOT_PATH, "bin", "exponential_MPI.exe")

serial_time = 1.0
for N in range(1, 11):
    value = 0.0
    average_time_taken = 0.0
    for i in range(NUM_TRIALS):
        command = f"mpiexec -n {N} {EXECUTABLE_PATH} -x {X}"
        result = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE)
        if result.stdout is not None:
            message = result.stdout.read().decode("utf-8")
            lines = message.split("\n")
            if i == 0:
                value = float(lines[0].split(":")[1].strip())
            time_taken = float(lines[1].split(":")[1].strip())
            average_time_taken += time_taken
        else:
            average_time_taken = -1
            break
    expt = math.e ** X
    diff = abs(value - expt)
    if diff > EPSILON:
        print(f"Accuracy error of: |{value} - {expt}| =  {diff}")
    average_time_taken /= NUM_TRIALS
    if N == 1:
        serial_time = average_time_taken
    speedup = serial_time / average_time_taken
    efficiency = speedup * 100 / N
    print(
        "N=%d: time taken = %.9f    speedup = %.4f    efficiency = %.4f"
        % (N, average_time_taken, speedup, efficiency)
    )
