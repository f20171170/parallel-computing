#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "mpi.h"

#define NUM_TERMS 2000000
#define USAGE_MESSAGE "Example usage: mpiexec -n 4 ./a.out -x 25\n"

int read_inputs(int argc, char **argv, int *x) {
	int x_mode = 0;
	for (int i = 1; i < argc; ++i) {
		char *arg = argv[i];
		switch (x_mode) {
		case 0:
			if (strcmp(arg, "-x") == 0) {
				x_mode = 1;
				break;
			}
			return 1;
		default:
			*x = atoi(arg);
			return 0;
		}
	}
	return 1;
}

/*
 * Our solution will involve a mixture of dynamic programming and parallel
 * programming. Each consecutive term in the series varies by a factor of
 * x/i where i is the index of the term. Performing just one multiplication
 * and one division operation per term instead of calculating each term from
 * the ground up would greatly improve the speed of the program execution.
 *
 * Inputs:
 *   x : The exponent in e ** x.
 *   n : The number of parallel processes to run.
 *   r : The rank of the current process.
 *
 * Output:
 *   The value of e ** x.
 */
double solve(int x, int n, int r) {
	int qu, rm, st_idx, end_idx;
	double term, result;
	double send_buffer[2], recv_buffer[2];
	MPI_Status status;

	if (x == 0)
		return 1;

	// Step 1: Decide the starting and ending points of the interval
	// of terms this particular process will have to calculate (bucketing).
	qu = NUM_TERMS / n;
	rm = NUM_TERMS % n;
	st_idx = r * qu;
	end_idx = st_idx + qu; // (r + 1) * qu
	if (r == n)
		end_idx += rm;

	// Step 2: Do the parallel number crunching.
	term = 1;
	result = term;
	for (int i = st_idx + 1; i < end_idx; ++i) {
		term *= (double)x / i;
		result += term;
	}
	// First term of the next bucket (yet to be multiplied by the common factor):
	term *= (double)x / end_idx;

	// Step 3: Do the reduction (multiply result with common factor and add
	// sum of previous bucket).
	if (r != 0) {
		MPI_Recv(&recv_buffer, 2, MPI_DOUBLE, r - 1, 0, MPI_COMM_WORLD,
			 &status);
		result = result * recv_buffer[0] + recv_buffer[1];
		term = term * recv_buffer[0];
	}
	if (r != n - 1) {
		send_buffer[0] = term;
		send_buffer[1] = result;
		MPI_Send(&send_buffer, 2, MPI_DOUBLE, r + 1, 0, MPI_COMM_WORLD);
	}

	return result;
}

int main(int argc, char **argv) {
	int x, n, r, err;
	double solution, time;

	err = read_inputs(argc, argv, &x);

	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &n);
	MPI_Comm_rank(MPI_COMM_WORLD, &r);

	if (err) {
		MPI_Finalize();
		if (r == 0)
			printf(USAGE_MESSAGE);
		return 1;
	}

	time = MPI_Wtime();
	solution = solve(x, n, r);
	time = MPI_Wtime() - time;

	MPI_Finalize();

	if (r == n - 1)
		printf("Result: %f\nTime Taken: %f\n", solution, time);

	return 0;
}
